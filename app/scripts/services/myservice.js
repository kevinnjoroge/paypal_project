'use strict';

/**
 * @ngdoc service
 * @name paypalApp.myService
 * @description
 * # myService
 * Service in the paypalApp.
 */
angular.module('paypalApp')
  .service('myService', function () {
    // AngularJS will instantiate a singleton by calling "new" on this function
  });
