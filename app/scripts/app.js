'use strict';

/**
 * @ngdoc overview
 * @name paypalApp
 * @description
 * # paypalApp
 *
 * Main module of the application.
 */
var paypalApp = angular
  .module('paypalApp', [
    'ui.router',
    'ngAnimate',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ngMaterial'
  ])
  .config(function ($stateProvider, $urlRouterProvider) {

    // send users to the form page
    $urlRouterProvider.otherwise('/login');

    $stateProvider
      .state('login', {
        url: '/login',
        templateUrl: 'views/loginform.html',
        controller: 'loginController'
      })
      .state('register', {
        url: '/register',
        templateUrl: '../views/KYCform.html',
        controller: 'registerController'
      })
      .state('resetPwd', {
        url: '/password_reset',
        templateUrl: 'views/resetPassword.html',
        controller: 'resetPasswordController'
      })
      .state('regionNation', {
        url: '/profile',
        templateUrl: 'views/regNationality.html',
        controller: 'registerController'
      })
      .state('OTPform', {
        url: '/register_profile',
        templateUrl: 'views/loginOTP.html',
        controller: 'registerController'
      })
      .state('sucurityQuestion', {
        url: '/security_question',
        templateUrl: 'views/securityQuestion.html',
        controller: 'registerController'
      })
      .state('dashboard', {
        url: '/dashboard',
        templateUrl: 'views/dashboard.html',
        controller: 'dashboardController'
      })
      .state('transactionHistory', {
        url: '/transactions',
        templateUrl: 'views/transactionHistory.html',
        controller: 'transactionController'
      })
      .state('withdraw', {
        url: '/paypall_withdraw',
        templateUrl: 'views/withdraw.html',
        controller: 'withdrawController'
      })
      .state('userProfile', {
        url: '/settings',
        templateUrl: 'views/profile.html',
        controller: 'profileController'
      })

      .state('paypallActs', {
        url: '/paypall_account',
        templateUrl: 'views/paypallAccounts.html',
        controller: 'paypallAccountsController'
      })

      .state('bankActs', {
        url: '/bank_account',
        templateUrl: 'views/bankAccounts.html',
        controller: 'bankAccountsController'
      })


      // url will be /form.credentials

  });
