'use strict';

/**
 * @ngdoc function
 * @name paypalApp.controller:paypallAccountsController
 * @description
 * # paypallAccountsController
 * Controller of the paypalApp
 */

paypalApp.controller('paypallAccountsController', function ($scope,$mdDialog) {


  $scope.ppActs = [
    { date: 'Cali Roll', act: 'one@yahoo.com' },
    { date: 'Philly',    act: 'two@yahoo.com' },
    { date: 'Tiger',     act: 'three@yahoo.com' },
    { date: 'Rainbow',   act: 'four@yahoo.com' }
  ];

  $scope.showAddPayPall = function(){
    $mdDialog.show({
      controller: addPayPallController,
      templateUrl: 'views/addPayPallAct.html'
    });
  };

  function addPayPallController($mdDialog, $scope){
    $scope.cancel = function(){
      $mdDialog.cancel();
    }

    $scope.update = function(user){
      var newPayPalAct = user.newPayPal;
      console.log(newPayPalAct);
      $mdDialog.cancel();

    }

  }

  });
