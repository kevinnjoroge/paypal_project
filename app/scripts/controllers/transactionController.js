'use strict';

/**
 * @ngdoc function
 * @name paypalApp.controller:transactionController
 * @description
 * # transactionController
 * Controller of the paypalApp
 */

paypalApp.controller('transactionController', function ($scope) {

  $scope.populateTransactions = [
    {id: 1, date: '02/11/2014',    bankact: 28080000008898989, paypall: 'one@this.com', amount: 2000.00 },
    {id: 2, date: '27/04/2015',    bankact: 48540804850854544, paypall: 'two@this.com', amount: 34450.00 },
    {id: 3, date: '14/06/2015',    bankact: 77668979794754795, paypall: 'three@this.com', amount: 9000.00 },
    {id: 4, date: '24/07/2015',    bankact: 67686676247847386, paypall: 'four@this.com', amount: 567.00 },
    {id: 5, date: '01/08/2015',    bankact: 77668979794754795, paypall: 'five@this.com', amount: 2400.00 },
    {id: 6, date: '04/08/2015',    bankact: 77668979794754795, paypall: 'six@this.com', amount: 210780.00 }
  ];


  });
