/**
 * Created by KevinKnight on 7/27/2015.
 */
'use strict';

/**
 * @ngdoc function
 * @name paypalApp.controller:registerController
 * @description
 * # registerController
 * Controller of the paypalApp
 */

paypalApp.controller('registerController', function ($scope, $http, $state, $rootScope) {

  $rootScope.showNationalIDInput = false;
  $rootScope.showPassPortInput = false;
  $rootScope.showSubIDInput = false;
  $rootScope.showContinueButton = false;

  $rootScope.showPwd = false;
  $rootScope.showOTPErrMsg = false;
  $rootScope.showOTPInput = true;

  $('#datepicker').datepicker({
    format: 'mm-dd-yyyy'
  });

  /*****load Nationality options*****************************************/
  $scope.loadNationality = function(){
    $scope.nationality = [
      { id: 0, name: 'Afghan' },
      { id: 1, name: 'Kenyan' },
      { id: 2, name: 'Albanian' },
      { id: 3, name: 'Algerian' },
      { id: 4, name: 'American' },
      { id: 5, name: 'Andorran' },
      { id: 4, name: 'Angolan' },
      { id: 4, name: 'Antiguans' },
      { id: 4, name: 'Argentinean' },
      { id: 4, name: 'Australian' },
      { id: 4, name: 'Austrian' },
      { id: 4, name: 'Azerbaijani' },
      { id: 4, name: 'Bahamian' },
      { id: 4, name: 'Bahraini' },
      { id: 4, name: 'Bangladeshi' },
      { id: 4, name: 'Barbadian' },
      { id: 4, name: 'Barbudans' },
      { id: 4, name: 'Batswana' }
    ];
    $scope.nationality = [
      { id: 0, name: 'Afghan' },
      { id: 1, name: 'Kenyan' },
      { id: 2, name: 'Albanian' },
      { id: 3, name: 'Algerian' },
      { id: 4, name: 'American' },
      { id: 5, name: 'Andorran' },
      { id: 4, name: 'Angolan' },
      { id: 4, name: 'Antiguans' },
      { id: 4, name: 'Argentinean' },
      { id: 4, name: 'Australian' },
      { id: 4, name: 'Austrian' },
      { id: 4, name: 'Azerbaijani' },
      { id: 4, name: 'Bahamian' },
      { id: 4, name: 'Bahraini' },
      { id: 4, name: 'Bangladeshi' },
      { id: 4, name: 'Barbadian' },
      { id: 4, name: 'Barbudans' },
      { id: 4, name: 'Batswana' }
    ];
  };

  /******load Residence options*****************************************/
  $scope.loadResidence = function(){
    $scope.residence = [
      { name: 'Kenya' },
      { name: 'Southern Sudan' },
      { name: 'Rwanda' },
      { name: 'Tanzania' },
      { name: 'Uganda' }
    ];
  };

  /********load Security Questions**********/
  $scope.loadQuestions = function(){
    $scope.secQuest = [
      { question: 'What street did you live on in third grade?' },
      { question: 'What was the name of your first stuffed animal?' },
      { question: 'In what city or town did your mother and father meet?' },
      { question: 'What was the last name of your third grade teacher?' },
      { question: 'In what city or town was your first job?' },
      { question: 'Where were you when you first heard about 9/11?' }
    ];
  }

/*****Step 1_a: Check selected Nationality and Residence*******/
  $scope.checkResidence = function(user){

      var userResidence = user.residence;
      var userNationality = user.nationality;

      if(angular.equals(userResidence, 'Uganda' || 'Tanzania' || 'Southern Sudan' || 'Rwanda') && (userNationality !== 'Uganda' || 'Tanzania' || 'Southern Sudan' || 'Rwanda')){
        $rootScope.showSubIDInput = true;
        $rootScope.showPassPortInput = true;
        $rootScope.showContinueButton = true;
      }else if(angular.equals(userResidence, 'Uganda' || 'Tanzania' || 'Southern Sudan' || 'Rwanda') && (userNationality, 'Uganda' || 'Tanzania' || 'Southern Sudan' || 'Rwanda')){
        $rootScope.showSubIDInput = true;
        $rootScope.showContinueButton = true;
      }


  };

/*****Step 1_b: Check selected Nationality and Residence*******/

  $scope.checkNationRegion =  function(user){
    var profile = {};
     profile.nationality = user.nationality;
     profile.residence = user.residence;

     profile.nationalID = user.nationalID;
     profile.passportNo = user.passportNo;
     profile.subsidiaryID = user.subsidiaryID;

      $state.go('register');

  };
/*****Step 2: Display / Get KYC Details of User*******/
  $scope.userKYC = function(){
   /* var userData = {};
        userData.firstName = user.firstName;
        userData.lastName = user.lastName;
        userData.email = user.email;
        userData.phone = user.phone;
        userData.dob = user.dob;
        userData.gender = user.gender;*/

    $state.go('OTPform');
  };


  $scope.getPassword = function(){
    $state.go('sucurityQuestion');
  };

  $scope.validateOTP = function(userOTP){
    var userotp = userOTP;
    if(angular.equals(userotp,"123456")){
      $('#OTPField').fadeOut(2000);
      $rootScope.showOTPInput = false;
      $('#pwdField').fadeIn(1500);
      $rootScope.showPwd = true;
    }else{
      $("#OTPError").fadeIn(1000);
      $rootScope.showOTPErrMsg = "One Time Password is incorrect!";
      $("#OTPError").fadeOut(7000);
    }
  };

/******validate sign up: step 1 + Capture details************************/

    $scope.checkSecurityQuestions = function(){

      $state.go('dashboard');

    }

/*****Function: Populate KYC Form Fields*******/
  function populateKYC(){

  }


});
