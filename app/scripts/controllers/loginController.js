/**
 * Created by KevinKnight on 7/30/2015.
 */
'use strict';

/**
 * @ngdoc function
 * @name paypalApp.controller:loginController
 * @description
 * # loginController
 * Controller of the paypalApp
 */

paypalApp.controller('loginController', function ($scope, $http, $state, $rootScope) {
  $rootScope.showDashHeader = false;
  $rootScope.showMsg = false;

  /*****validate login*****************************************/
  $scope.authorizeLogin = function(user){

    var loginCredentials = {}
    loginCredentials.username = user.username;
    loginCredentials.password = user.password;


    if(loginCredentials.username == "kevin" && loginCredentials.password == "knight"){
      $state.go('dashboard');
    }else{
      $rootScope.showMsg = true;
      $("#loginError").fadeIn(1000);
      $scope.errorMsg = "Username / Password Incorrect";
      $("#loginError").fadeOut(7000);
    }
  }
});





