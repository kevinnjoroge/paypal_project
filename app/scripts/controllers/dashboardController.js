'use strict';

/**
 * @ngdoc function
 * @name paypalApp.controller:dashboardController
 * @description
 * # dashboardController
 * Controller of the paypalApp
 */

  paypalApp.controller('dashboardController', function ($scope, $http, $state, $rootScope) {

  /*********Load bank accounts**************/
      $scope.loadBankAccounts = function(){
        $scope.bankAccounts = [
          { act: '01877355328888' },
          { act: '00850546750659' },
          { act: '02727477235675' }
        ];
      }
  /*********Load pay pall accounts**************/
    $scope.loadPayPallAccounts = function(){
      $scope.bankAccounts = [
        { act: '01877355328888' },
        { act: '00850546750659' },
        { act: '02727477235675' }
      ];
    }


  });
