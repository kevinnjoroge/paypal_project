'use strict';

describe('Controller: BankaccountscontrollerCtrl', function () {

  // load the controller's module
  beforeEach(module('paypalApp'));

  var BankaccountscontrollerCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    BankaccountscontrollerCtrl = $controller('BankaccountscontrollerCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(BankaccountscontrollerCtrl.awesomeThings.length).toBe(3);
  });
});
