'use strict';

describe('Controller: OpenaccountcontrollerCtrl', function () {

  // load the controller's module
  beforeEach(module('paypalApp'));

  var OpenaccountcontrollerCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    OpenaccountcontrollerCtrl = $controller('OpenaccountcontrollerCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(OpenaccountcontrollerCtrl.awesomeThings.length).toBe(3);
  });
});
