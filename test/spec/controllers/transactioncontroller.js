'use strict';

describe('Controller: TransactioncontrollerCtrl', function () {

  // load the controller's module
  beforeEach(module('paypalApp'));

  var TransactioncontrollerCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    TransactioncontrollerCtrl = $controller('TransactioncontrollerCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(TransactioncontrollerCtrl.awesomeThings.length).toBe(3);
  });
});
