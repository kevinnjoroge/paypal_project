'use strict';

describe('Controller: LinkaccountscontrollerCtrl', function () {

  // load the controller's module
  beforeEach(module('paypalApp'));

  var LinkaccountscontrollerCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    LinkaccountscontrollerCtrl = $controller('LinkaccountscontrollerCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(LinkaccountscontrollerCtrl.awesomeThings.length).toBe(3);
  });
});
