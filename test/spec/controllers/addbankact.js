'use strict';

describe('Controller: AddbankactCtrl', function () {

  // load the controller's module
  beforeEach(module('paypalApp'));

  var AddbankactCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AddbankactCtrl = $controller('AddbankactCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(AddbankactCtrl.awesomeThings.length).toBe(3);
  });
});
