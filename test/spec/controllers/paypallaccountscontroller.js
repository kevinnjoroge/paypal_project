'use strict';

describe('Controller: PaypallaccountscontrollerCtrl', function () {

  // load the controller's module
  beforeEach(module('paypalApp'));

  var PaypallaccountscontrollerCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    PaypallaccountscontrollerCtrl = $controller('PaypallaccountscontrollerCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(PaypallaccountscontrollerCtrl.awesomeThings.length).toBe(3);
  });
});
