'use strict';

describe('Controller: ResetpasswordcontrollerCtrl', function () {

  // load the controller's module
  beforeEach(module('paypalApp'));

  var ResetpasswordcontrollerCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ResetpasswordcontrollerCtrl = $controller('ResetpasswordcontrollerCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(ResetpasswordcontrollerCtrl.awesomeThings.length).toBe(3);
  });
});
